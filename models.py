from sqlalchemy import func
from flask_restplus import fields
from db import db


class TaskModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50))
    text = db.Column(db.String(250))
    timestamp = db.Column(db.DateTime, server_default=func.now())


def flask_task_model(namespace):
    model = {
        'id': fields.Integer(),
        'username': fields.String(),
        'text': fields.String(),
        'timestamp': fields.DateTime()
        }
    return namespace.model('Task', model)
