from flask import Flask
from flask_restplus import Api, Namespace

from services import init_task_service
from db import db, db_config


def create_app():
    app = Flask(__name__)
    api = Api(app, version='0.1',
              title='Tasks by Nossin',
              description='A Simple tasks system')

    api_namespace = Namespace('api', description='API operations')
    api.add_namespace(api_namespace)

    app.config['RESTPLUS_MASK_SWAGGER'] = False
    app.config.update(db_config)

    db.init_app(app)
    app.db = db

    init_task_service(api_namespace)

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
