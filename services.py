
import http.client

from flask import abort
from datetime import datetime
from flask_restplus import Resource

from models import TaskModel, flask_task_model
from token_validation import validate_token_header
import config as config
from db import db


def authentication_header_parser(value):
    username = validate_token_header(value, config.PUBLIC_KEY)
    if username is None:
        abort(401)
    return username


def init_task_service(namespace):
    task_model = flask_task_model(namespace)

    authentication_parser = namespace.parser()
    authentication_parser.add_argument('Authorization', location='headers',
                                       type=str,
                                       help='Bearer Access Token')

    task_parser = authentication_parser.copy()
    task_parser.add_argument('text', type=str, required=True,
                             help='The task')

    init_task_routes(namespace, task_model, authentication_parser, task_parser)


def init_task_routes(namespace, task_model,
                     authentication_parser, task_parser):

    @namespace.route('/me/tasks/')
    class MeTaskListCreate(Resource):

        @namespace.doc('list_tasks')
        @namespace.expect(authentication_parser)
        @namespace.marshal_with(task_model, as_list=True)
        def get(self):
            args = authentication_parser.parse_args()
            username = authentication_header_parser(args['Authorization'])

            tasks = (TaskModel
                     .query
                     .filter(TaskModel.username == username)
                     .order_by('id')
                     .all())
            return tasks

        @namespace.doc('create_task')
        @namespace.expect(task_parser)
        @namespace.marshal_with(task_model, code=http.client.CREATED)
        def post(self):
            args = task_parser.parse_args()
            username = authentication_header_parser(args['Authorization'])

            new_task = TaskModel(username=username,
                                 text=args['text'],
                                 timestamp=datetime.utcnow())
            db.session.add(new_task)
            db.session.commit()

            result = namespace.marshal(new_task, task_model)

            return result, http.client.CREATED

    search_parser = namespace.parser()
    search_parser.add_argument('search', type=str, required=False,
                               help='Search text')

    @namespace.route('/tasks/')
    class TasksList(Resource):

        @namespace.doc('list_tasks')
        @namespace.marshal_with(task_model, as_list=True)
        @namespace.expect(search_parser)
        def get(self):
            args = search_parser.parse_args()
            search_param = args['search']
            query = TaskModel.query
            if search_param:
                query = (query.filter(TaskModel.text.contains(search_param)))

            query = query.order_by('id')
            tasks = query.all()

            return tasks

    @namespace.route('/tasks/<int:task_id>/')
    class TaskRetrieve(Resource):

        @namespace.doc('retrieve_task')
        @namespace.marshal_with(task_model)
        def get(self, task_id):
            task = TaskModel.query.get(task_id)
            if not task:
                return '', http.client.NOT_FOUND

            return task
