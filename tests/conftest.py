
import pytest
import http.client
from app import create_app
from constants import PRIVATE_KEY
import token_validation
from faker import Faker
fake = Faker()


@pytest.fixture
def app():
    application = create_app()

    application.app_context().push()
    # Initialise the DB
    application.db.create_all()

    return application


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def task_fixture(client):
    '''
    Generate three tasks in the system.
    '''

    task_ids = []
    for _ in range(3):
        task = {
            'text': fake.text(240),
        }
        header = token_validation.generate_token_header(fake.name(),
                                                        PRIVATE_KEY)
        headers = {
            'Authorization': header,
        }
        response = client.post('/api/me/tasks/', data=task,
                               headers=headers)
        assert http.client.CREATED == response.status_code
        result = response.json
        task_ids.append(result['id'])

    yield task_ids
