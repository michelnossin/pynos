'''
Test the Task operations


'''
from unittest.mock import ANY
import http.client
from freezegun import freeze_time
from constants import PRIVATE_KEY
import token_validation

from faker import Faker
fake = Faker()


@freeze_time('2019-05-07 13:47:34')
def test_create_me_task(client):
    new_task = {
        'username': fake.name(),
        'text': fake.text(240),
    }
    header = token_validation.generate_token_header(fake.name(), PRIVATE_KEY)
    headers = {
        'Authorization': header,
    }
    response = client.post('/api/me/tasks/', data=new_task,
                           headers=headers)
    result = response.json

    assert http.client.CREATED == response.status_code

    expected = {
        'id': ANY,
        'username': ANY,
        'text': new_task['text'],
        'timestamp': '2019-05-07T13:47:34',
    }
    assert result == expected


def test_create_me_unauthorized(client):
    new_task = {
        'username':  fake.name(),
        'text': fake.text(240),
    }
    response = client.post('/api/me/tasks/', data=new_task)
    assert http.client.UNAUTHORIZED == response.status_code


def test_list_me_tasks(client, task_fixture):
    username = fake.name()
    text = fake.text(240)

    new_task = {
        'text': text,
    }
    header = token_validation.generate_token_header(username, PRIVATE_KEY)
    headers = {
        'Authorization': header,
    }
    response = client.post('/api/me/tasks/', data=new_task,
                           headers=headers)
    result = response.json

    assert http.client.CREATED == response.status_code

    response = client.get('/api/me/tasks/', headers=headers)
    results = response.json

    assert http.client.OK == response.status_code
    assert len(results) == 1
    result = results[0]
    expected_result = {
        'id': ANY,
        'username': username,
        'text': text,
        'timestamp': ANY,
    }
    assert result == expected_result


def test_list_me_unauthorized(client):
    response = client.get('/api/me/tasks/')
    assert http.client.UNAUTHORIZED == response.status_code


def test_list_tasks(client, task_fixture):
    response = client.get('/api/tasks/')
    result = response.json

    assert http.client.OK == response.status_code
    assert len(result) > 0

    # Check that the ids are increasing
    previous_id = -1
    for task in result:
        expected = {
            'text': ANY,
            'username': ANY,
            'id': ANY,
            'timestamp': ANY,
        }
        assert expected == task
        assert task['id'] > previous_id
        previous_id = task['id']


def test_list_tasks_search(client, task_fixture):
    username = fake.name()
    new_task = {
        'username': username,
        'text': 'A tale about a Platypus'
    }
    header = token_validation.generate_token_header(username, PRIVATE_KEY)
    headers = {
        'Authorization': header,
    }
    response = client.post('/api/me/tasks/', data=new_task,
                           headers=headers)
    assert http.client.CREATED == response.status_code

    response = client.get('/api/tasks/?search=platypus')
    result = response.json

    assert http.client.OK == response.status_code
    assert len(result) > 0

    # Check that the returned values contain "platypus"
    for task in result:
        expected = {
            'text': ANY,
            'username': username,
            'id': ANY,
            'timestamp': ANY,
        }
        assert expected == task
        assert 'platypus' in task['text'].lower()


def test_get_task(client, task_fixture):
    task_id = task_fixture[0]
    response = client.get(f'/api/tasks/{task_id}/')
    result = response.json

    assert http.client.OK == response.status_code
    assert 'text' in result
    assert 'username' in result
    assert 'timestamp' in result
    assert 'id' in result


def test_get_non_existing_task(client, task_fixture):
    task_id = 123456
    response = client.get(f'/api/tasks/{task_id}/')

    assert http.client.NOT_FOUND == response.status_code
